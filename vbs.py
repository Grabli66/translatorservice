#!/usr/bin/python

from subprocess import call

def build():
    call(["valac", "./src/Main.vala", "./src/WebJsonClient.vala", "./src/TranslatorSourceYandex.vala", "./src/DataCache.vala", "./src/TextProcessor.vala", "./src/TextTranslator.vala",
    "--pkg=json-glib-1.0", "--pkg=libsoup-2.4", "--pkg=gee-0.8", "--pkg=valum",
    "-b", "./src", "-d", "./install", "-o", "service"])
    
build()
