public class TranslatorSourceYandex {
    private const string API_KEY = "trnsl.1.1.20150427T160217Z.69da78079263823e.0f0caa9724cee126c18028e70f227b633a3fe58f";
    
    public static string Translate(string from, string to, string text) {
        var ntext = Soup.URI.encode(text, null);
        var request = @"https://translate.yandex.net/api/v1.5/tr.json/translate?key=$(API_KEY)&lang=$(from)-$(to)&text=$(text)";
        var root = WebJsonClient.Get(request);        
        
        var data = new Gee.ArrayList<string>();
        if (root != null) {
            var sentences = root.get_array_member("text");

            if (sentences != null) {
                foreach (var s in sentences.get_elements()) {
                    var el = s.get_string();
                    data.add(el);
                }
            }
            
            return data.to_array()[0];
        }
                
        return null;
    }    
}
