class TranslatorSourceDispatcher {
    private static TranslatorSourceDispatcher _instance;
    
    private TranslatorSourceDispatcher() {}
    
    public static TranslatorSourceDispatcher GetInstance() {
        if (_instance == null) {
            _instance = new TranslatorSourceDispatcher();
        }
        return _instance;
    }
    
    public ITranslatorSource GetFreeSource() {
        return null;
    }
}
