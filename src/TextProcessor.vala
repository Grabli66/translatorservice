class TextPart {
    public string[] Words;
    public string Join() {
        return "";
    }
}

class TextProcessor {
    private static TextProcessor _instance;
    
    private TextProcessor() {}
    
    public static TextProcessor GetInstance() {
        if (_instance == null) {
            _instance = new TextProcessor();
        }
        return _instance;
    }
    
    /*
    *   Splits text by word count
    */
    public TextPart[] SplitWordsByCount(string text, int count) {
        var dat = text.split(" ");
        var len = dat.length;
        if (len <= count) return new TextPart[] { 
            new TextPart() {
                words = new string[] { text }
            }
        };
        
        var sb = new StringBuilder();        
        var counter = 0;
        var res = new Gee.ArrayList<TextPart>();
        var words = new Gee.ArrayList<string>();
        
        for (var i = 0; i < len; i++) {                                             
            counter += 1;
            words.add(dat[i]);
            
            if (counter >= count || i >= len - 1) {                
                var tp = new TextPart();
                tp.Words = words.to_array();
                res.add(tp);                
                counter = 0;
            }                        
        }                

        return res.to_array();
    }
}
