class DataCache {
    private Gee.HashMap<string, string> _cache = new Gee.HashMap<string, string>();
    private static DataCache _instance;
    
    private DataCache() {}
    
    public static DataCache GetInstance() {
        if (_instance == null) {
            _instance = new DataCache();
        }
        return _instance;
    }
    
    public string Get(string key) {        
        return _cache.@get(key);
    }
    
    public void Set(string key, string value) {
        _cache.@set(key, value);
    }
}