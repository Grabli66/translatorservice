using Valum;
using VSGI.Soup;

var app = new Router ();

app.get("translate/<from>/<to>/<any:text>", (req, res) => {
    var tr = TextTranslator.GetInstance();
    var text = tr.Translate(req.params["from"], req.params["to"], req.params["text"]);                        
    res.body.write_all (text.data, null);
});

new Server ("org.valum.example.App", app.handle).run ({"app","--server", "192.168.56.101", "--port", "3003"});
