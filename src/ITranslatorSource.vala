interface ITranslatorSource : GLib.Object {
    string Translate(string from, string to, string text);
}
