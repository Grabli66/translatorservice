class TextTranslator {
    private static TextTranslator _instance;
    
    private TextTranslator() {}
    
    public static TextTranslator GetInstance() {
        if (_instance == null) {
            _instance = new TextTranslator();
        }
        return _instance;
    }
    
    private const int WORD_COUNT = 2;
    
    public string Translate(string from, string to, string text) {    
        var c = DataCache.GetInstance();
        var tp = TextProcessor.GetInstance();        
        var parts = tp.SplitWordsByCount(text, WORD_COUNT);
        foreach (var p in parts) {
            var cs = c.Get((p.Join()));
            if (cs == null) {
                
            }            
        }        
          
        return "";
    }
}
